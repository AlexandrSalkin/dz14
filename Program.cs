﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ДЗ14
{
    class Program
    {
        class Vertex<T>
        {
            List<Vertex<T>> neighbors;
            T value;
            bool isVisited;

            public List<Vertex<T>> Neighbors { get { return neighbors; } set { neighbors = value; } }
            public T Value { get { return value; } set { this.value = value; } }
            public bool IsVisited { get { return isVisited; } set { isVisited = value; } }

            public Vertex(T value)
            {
                this.value = value;
                isVisited = false;
                neighbors = new List<Vertex<T>>();
            }

            public void Visit()
            {
                isVisited = true;
            }
 
            public void AddEdges(List<Vertex<T>> newNeighbors)
            {
                neighbors.AddRange(newNeighbors);
            }

            public override string ToString()
            {
                StringBuilder allNeighbors = new StringBuilder("");
                allNeighbors.Append(value + ": ");

                foreach (Vertex<T> neighbor in neighbors)
                {
                    allNeighbors.Append(neighbor.value + "  ");
                }

                return allNeighbors.ToString();
            }
        }

        class UndirectedGenericGraph<T>
        {
            private List<Vertex<T>> vertices;

            int size;

            public UndirectedGenericGraph(List<Vertex<T>> initialNodes)
            {
                vertices = initialNodes;
                size = vertices.Count;
            }
       
            public void DepthFirstSearch(Vertex<T> root)
            {
                if (!root.IsVisited)
                {
                    Console.Write(root.Value + " ");
                    root.Visit();

                    foreach (Vertex<T> neighbor in root.Neighbors)
                    {
                        DepthFirstSearch(neighbor);
                    }
                }
            }

            public void BreadthFirstSearch(Vertex<T> root)
            {
                Queue<Vertex<T>> queue = new Queue<Vertex<T>>();

                root.Visit();

                queue.Enqueue(root);

                while (queue.Count > 0)
                {
                    Vertex<T> current = queue.Dequeue();

                    foreach (Vertex<T> neighbor in current.Neighbors)
                    {
                        if (!neighbor.IsVisited)
                        {
                            Console.Write(neighbor.Value + " ");
                            neighbor.Visit();
                            queue.Enqueue(neighbor);
                        }
                    }
                }
            }
        }

        static void Main(string[] args)
        {
            List<Vertex<string>> vertices = new List<Vertex<string>>
            (
                new Vertex<string>[]
                {
                new Vertex<string>("a"),
                new Vertex<string>("b"),
                new Vertex<string>("c"),
                new Vertex<string>("d"),
                new Vertex<string>("e"),
                new Vertex<string>("f"),
                new Vertex<string>("g"),
                new Vertex<string>("h")
                }
            );

            vertices[0].AddEdges(new List<Vertex<string>>(new Vertex<string>[]
            {
            vertices[1], vertices[2], vertices[3]
            }));

            vertices[1].AddEdges(new List<Vertex<string>>(new Vertex<string>[]
            {
            vertices[0], vertices[4], vertices[5]
            }));

            vertices[2].AddEdges(new List<Vertex<string>>(new Vertex<string>[]
            {
            vertices[0], vertices[6]
            }));

            vertices[3].AddEdges(new List<Vertex<string>>(new Vertex<string>[]
            {
            vertices[0]
            }));

            vertices[4].AddEdges(new List<Vertex<string>>(new Vertex<string>[]
            {
            vertices[1]
            }));

            vertices[5].AddEdges(new List<Vertex<string>>(new Vertex<string>[]
            {
            vertices[1]
            }));

            vertices[6].AddEdges(new List<Vertex<string>>(new Vertex<string>[]
            {
            vertices[2], vertices[7]
            }));

            vertices[7].AddEdges(new List<Vertex<string>>(new Vertex<string>[]
            {
            vertices[6]
            }));

            UndirectedGenericGraph<string> testGraph = new UndirectedGenericGraph<string>(vertices);

            foreach (Vertex<string> vertex in vertices)
            {
                Console.WriteLine(vertex.ToString());
                Console.WriteLine(vertex.ToString().Contains("h"));
            }

            //testGraph.DepthFirstSearch(vertices[2]);
            testGraph.BreadthFirstSearch(vertices[6]);
        }
    }
}
